package com.egamt.app;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream.GetField;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


public class BootClass {

	private static Map<Integer,String> difficulties = new HashMap<Integer, String>();
	private static Map<Integer,String> tags = new HashMap<>();
	
	private static List<Integer> tag1=new ArrayList<>();
	private static List<Integer> tag2=new ArrayList<>();
	private static List<Integer> tag3=new ArrayList<>();
	private static List<Integer> tag4=new ArrayList<>();
	private static List<Integer> tag5=new ArrayList<>();
	private static List<Integer> tag6=new ArrayList<>();
	
	private static List<List<Integer>> quizzList=new ArrayList<>();
	
	private static List<Integer> quesList;
	private static List<Integer> quesAll=new ArrayList<>();
	//private static int[] tag1;
	static int duplicateIndex=0;
	static int hard,med,easy;
	
	static Boolean flag;
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("Input File: "+System.getProperty("user.dir")+"\\TestCase3.txt");
		
		try(InputStream fis=new FileInputStream(System.getProperty("user.dir")+"\\TestCase3.txt");
				InputStreamReader isr=new InputStreamReader(fis,StandardCharsets.UTF_8);
				BufferedReader br=new BufferedReader(isr);)
		{
			
			br.lines().forEach(line->{
				
				String tag=line.substring(line.lastIndexOf("|")+1);
		
				String level=line.substring(line.indexOf("|")+1,line.lastIndexOf("|"));
				int que=Integer.parseInt(line.substring(line.indexOf("Q")+1,line.indexOf("|")));
				
				if(tag.equals("Tag1")) {
					tag1.add(que);
				}
				if(tag.equals("Tag2")) {
					tag2.add(que);
				}
				if(tag.equals("Tag3")) {
					tag3.add(que);
				}if(tag.equals("Tag4")) {
					tag4.add(que);
				}
				if(tag.equals("Tag5")) {
					tag5.add(que);
				}
				if(tag.equals("Tag6")) {
					tag6.add(que);
				}
				
				
				difficulties.put(que,level);
				tags.put(que, tag);
				quesAll.add(que);
				
			});
		}
		System.out.println("Records Size:"+quesAll.size());
		
		/*
		 * difficulties.forEach((key,value)->{ System.out.println(key+":->"+value); });
		 * 
		 * tags.forEach((key,value)->{ System.out.println(key+":->"+value); });
		 */
		
		  /*System.out.println(quesList); System.out.println(quesList.size());*/
		
		
		
		
		
		while(duplicateIndex<2) {
			hard=0;
			med=0;
			easy=0;
			quesList=new ArrayList<>();
				
				//int randomIndex = rand.nextInt(tag1.size());
				quesList.add(tag1.get(getRandom(tag1.size())));
				
				quesList.add(tag2.get(getRandom(tag2.size())));
				quesList.add(tag3.get(getRandom(tag3.size())));
				quesList.add(tag4.get(getRandom(tag4.size())));
				quesList.add(tag5.get(getRandom(tag5.size())));
				quesList.add(tag6.get(getRandom(tag6.size())));
				
				
				
				hard=(int) quesList.stream().filter(que->difficulties.get(que).equals("HARD")).count();
				med=(int) quesList.stream().filter(que->difficulties.get(que).equals("MEDIUM")).count();
				easy=(int) quesList.stream().filter(que->difficulties.get(que).equals("EASY")).count();
				
				
				
				for(int j=hard;j<2;j++) {
					difficulties.forEach((que,level)->{
						if(!quesList.contains(que) && level.equals("HARD"))
						{	
							if(quesList.size()<10)
							quesList.add(que);
						}
					});
				}
				
				for(int j=med;j<2;j++) {
					difficulties.forEach((que,level)->{
						if(!quesList.contains(que) && level.equals("MEDIUM"))
						{	
							if(quesList.size()<10)
							quesList.add(que);
						}
					});
				}
				for(int j=easy;j<2;j++) {
					difficulties.forEach((que,level)->{
						if(!quesList.contains(que) && level.equals("EASY"))
						{	
							if(quesList.size()<10)
							quesList.add(que);
						}
					});
				}
				
				for(int j=quesList.size();j<10;j++) {
					quesAll.forEach(que->{
						if(!quesList.contains(que)) {
							if(quesList.size()<10) {
								quesList.add(que);
							}
						}
					});
				}
				
				/*quesList.forEach(que->{
					System.out.print(que+", ");
				});
				System.out.println();
				*/
				
				Collections.sort(quesList);
				flag=false;
				
				if(quizzList.size()>0) {
					quizzList.forEach(list->{
						Collections.sort(list);
						if(list.equals(quesList)) {
							flag=true;
						}
						
					});
				}
				
				if(flag) {
					duplicateIndex++;
				}
				else {
					quizzList.add(quesList);
					duplicateIndex=0;
				}
				
				
			//	System.out.println(quizzList.size());
				
			
				 
		}
		
		System.out.println(quizzList.size());
		

	}
	
	public static int getRandom(int size) {
		
		Random rand = new Random();
		
		return rand.nextInt(size);
	}

}
